import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import axios, { AxiosResponse } from 'axios';
import { forkJoin, Observable } from 'rxjs';
import { CreateItemDto } from './dto/create-item.dto';
import { QuerySearchDto } from './dto/query-validator';
import { UpdateItemDto } from './dto/update-item.dto';
import { map, mergeMap } from 'rxjs/operators';


@Injectable()
export class ItemsService {
  constructor(private httpService: HttpService
  ) { }

  create(createItemDto: CreateItemDto) {
    return 'This action adds a new item';
  }

  searchByQuery(query: QuerySearchDto): Observable<any> {
    const author = {
      name: "Daniel",
      lastname: "Arroyo"
    }
    return this.httpService.get(encodeURI(`https://api.mercadolibre.com/sites/MLA/search?q=${query}`)).pipe(
      map((axiosResponse: AxiosResponse) => {

        const items = axiosResponse.data.results.map(item => {
          const [amount, decimals] = item.price.toString().split(".");

          return {
            id: item.id,
            title: item.title,
            price: {
              currency: item.currency_id,
              amount: parseInt(amount),
              decimals: parseInt(decimals)
            },
            address: item.address.state_name,
            picture: item.thumbnail,
            condition: item.condition,
            free_shipping: item.shipping.free_shipping
          };
        }).slice(0, 4);

        const categories = axiosResponse.data.filters.length && axiosResponse.data.filters[0].values
          ? axiosResponse.data.filters[0].values[0].path_from_root.map(
            category => category.name
          )
          : [];

        return {
          author,
          categories,
          items
        }
      })
    );
  }

  searchById(id: string) {
    const author = {
      name: "Daniel",
      lastname: "Arroyo"
    }

    let item;
    let amount, decimals;
    let categories = []
    return this.httpService.get(encodeURI(`https://api.mercadolibre.com/items/${id}`)).pipe(
      // optional, but it makes working with responses easier

      map(resp => {
        item = resp.data;
        [amount, decimals] = item.price.toString().split(".");
      }),

      mergeMap(loginData => this.httpService.get(encodeURI(`https://api.mercadolibre.com/categories/${item.category_id}`))),

      map(resp => {
        categories.push(resp.data.name)
      }),

      // make new http call and switch to this observable
      mergeMap(loginData => this.httpService.get(encodeURI(`https://api.mercadolibre.com/items/${id}/description`))),

      map(resp => {
        return {
          author,
          item: {
            id: item.id,
            title: item.title,
            price: {
              currency: item.currency_id,
              amount: parseInt(amount),
              decimals: parseInt(decimals)
            },
            categories,
            picture: item.thumbnail,
            condition: item.condition,
            free_shipping: item.shipping.free_shipping,
            sold_quantity: item.sold_quantity,
            description: resp.data.plain_text
          }
        }
      }),
    )
  }
  findOne(id: number) {
    return `This action returns a #${id} item`;
  }

  update(id: number, updateItemDto: UpdateItemDto) {
    return `This action updates a #${id} item`;
  }

  remove(id: number) {
    return `This action removes a #${id} item`;
  }
}
