import { Module } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsController } from './items.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [ItemsController],
  imports: [HttpModule],
  providers: [ItemsService]
})
export class ItemsModule { }
