import { IsEmail, IsNotEmpty } from 'class-validator';

export class QuerySearchDto {
    @IsNotEmpty({ message: 'Cannot be empty' })
    q: string;
}